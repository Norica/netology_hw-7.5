        function submit(email, password) {
            var xhr = new XMLHttpRequest();

            var body = 'email=' + encodeURIComponent(email) +
              '&password=' + encodeURIComponent(password);

            xhr.open("POST", 'http://netology-hj-ajax.herokuapp.com/homework/login_json', true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

            xhr.onreadystatechange = function() {//Call a function when the state changes.
                if(xhr.readyState == 4) {
                    results(xhr.responseText);
                }
            }
            xhr.send(body);
        };
        var button = document.querySelector('[data-role="json-request-form-submit"]');

        button.onclick = function(e) {
            e.preventDefault();
            var email    = document.querySelector('[data-role="json-request-form-login"]').value;
            var password = document.querySelector('[data-role="json-request-form-password"]').value;
            submit(email, password);
        };

        function results(response) {
            var json = JSON.parse(response);
            if(json.error && json.error.code == 401) {
                alert(json.error.message);
            } else {
                console.log(json.age);
                console.log(json.country);
                console.log(json.lastname);
                console.log(json.name);
                console.log(json.userpic);
            }
        };
